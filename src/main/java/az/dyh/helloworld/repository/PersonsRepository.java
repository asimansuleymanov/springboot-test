package az.dyh.helloworld.repository;

import az.dyh.helloworld.data.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PersonsRepository extends JpaRepository<Person, Long> {
    @Query("SELECT p FROM Person p WHERE p.name = :name")
    List<Person> searchPersonForName(@Param("name") String name);
}
