package az.dyh.helloworld.service;

import az.dyh.helloworld.data.Person;
import az.dyh.helloworld.repository.PersonsRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PersonsService {

    private final PersonsRepository personsRepository;

    public PersonsService(PersonsRepository personsRepository) {
        this.personsRepository = personsRepository;
    }

    public List<Person> getAllPersons() {
        return personsRepository.findAll();
    }

    public Person getPersonById(Long id) {
        Optional<Person> person = personsRepository.findById(id);
        return person.isPresent() ? person.get() : null;
    }

    public Person createPersons(Person person) {
        return personsRepository.save(person);
    }

    public Person updatePersons(Long id, Person person) {
        Optional<Person> oldPerson = personsRepository.findById(id);
        if(oldPerson.isPresent()){
            return personsRepository.save(person);
        }
        return null;
    }

    public List<Person> findPersonByName(String name) {
        return personsRepository.searchPersonForName(name);
    }
}
