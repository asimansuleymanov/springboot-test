package az.dyh.helloworld.controller;

import az.dyh.helloworld.data.Person;
import az.dyh.helloworld.service.PersonsService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("api")
public class PersonsController {
    private final PersonsService personsService;

    public PersonsController(PersonsService personsService) {
        this.personsService = personsService;
    }

    @GetMapping("/persons")
    List<Person> getAllPersons(){
        return personsService.getAllPersons();
    }

    @GetMapping("/persons/find")
    List<Person> findPersonByName(@RequestParam String name){
        return personsService.findPersonByName(name);
    }

    @PostMapping("/persons")
    Person createPersons(@RequestBody Person person){
        return personsService.createPersons(person);
    }


    @PutMapping("/persons/{id}")
    Person createPersons(@RequestBody Person person, @PathVariable Long id){
        return personsService.updatePersons(id, person);
    }


    @GetMapping("/persons/{id}")
    Person getPersonById(@PathVariable Long id){
        return personsService.getPersonById(id);
    }

}
