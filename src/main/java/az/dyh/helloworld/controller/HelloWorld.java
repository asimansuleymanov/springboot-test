package az.dyh.helloworld.controller;

import az.dyh.helloworld.data.Person;
import az.dyh.helloworld.service.PersonsService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/page")
public class HelloWorld {
    private PersonsService personsService;

    public HelloWorld(PersonsService personsService) {
        this.personsService = personsService;
    }

    @RequestMapping("/home")
    public String getHome(Model model){
        model.addAttribute("listPerson", personsService.getAllPersons());
        return "hello-page";
    }
}
